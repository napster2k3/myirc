/**
 * Created by camillegreselle on 05/07/2014.
 */

/**
 * Common Functions
 */

var getChannel = function(array, pattern) {

    for (var count in array) {
        if(array[count].name == pattern)
            return array[count];
    }

    return false;
}

/**
 * Init nodeJS Server
 */

var server = require('http');
var md5 = require('MD5');

httpServer = server.createServer(function(request, output) {
    console.log('Server started');
});

httpServer.listen(1337);

/**
 * MySQL Instance
 */

var mysql = require('mysql');

var initSql = function () {
    var sql = mysql.createConnection({
        host     : '127.0.0.1',
        user     : 'root',
        password : 'litepanel',
        database : 'myirc',
        port     : 3306,
        socketPath : '/Applications/MAMP/tmp/mysql/mysql.sock'
    });

    sql.connect(function(error) {
        if(error)
            console.error('Unable to connect through mysql', error);
    });

    return sql;
}

/**
 * @var connectedUser
 * @type array
 */

connectedUser = [];


/**
 * User Object
 */


var user = function(id, email, nickname, password) {

    this.nickname = nickname;
    this.email = email;
    this.id = id;
    this.password = password;
};

function register(data) {
    var sql = initSql();
    var query = "INSERT INTO users (email, nickname, password) VALUE(" + sql.escape(data.email) + ", " + sql.escape(data.nickname) + ", '" + md5(sql.escape(data.password)) + "')";
    sql.query(query, function(error, result) {
        console.error(error);
    });
    sql.end();
}

/**
 * @object Channel
 * @type Object
 */

channelStack = [];

var Channel = function(name) {

    this.name = name;
    this.messages = [];
    this.connectedUser = [];
    this.admin = [];

    this.addMessage = function(message) {
        this.message.push(message);
    };

    this.addAdmin = function(user) {
        this.admin[user.id] = user;
    };

    this.addUser = function(user) {
        this.connectedUser[user.id] = user;
    };

    this.removeUser = function(user) {
        delete this.connectedUser[user.id];
    };
};

/**
 * Init Socket.io
 */

var io = require('socket.io').listen(httpServer);

io.sockets.on('connection', function(currentSocket) {

    /**
     * current Connected User (self sock)
     */

    var currentUser = false;

    // Array containing the channels where the user is connected

    connectedChannel = [];

    /**
     * getUserList
     */

    currentSocket.on('getUserList', function(channel) {
        var chan = getChannel(channelStack, channel);
        currentSocket.emit('fetchUserList', chan.connectedUser);
    });

    /**
     * Wizz
     */

    currentSocket.on('wizz', function(channel) {
        io.sockets.in(channel).emit('pushWizz', channel);
    });

    /**
     * getChannelList
     */

    currentSocket.on('getChannelList', function(pattern) {

        var cStack = [];
        console.log(pattern);
        for(var i in channelStack) {
            if(pattern == null)
                cStack.push(channelStack[i].name);
            else {
                if(channelStack[i].name.match(pattern))
                    cStack.push(channelStack[i].name);
            }
        }
        currentSocket.emit('fetchChannelList', cStack);
    });

    /**
     * Channel Room
     */

    currentSocket.on('leaveChannel', function(channel) {

        io.sockets.in(channel).emit('userleftchannel', currentUser.nickname);

    });

    currentSocket.on('sendMsg', function(data) {
        console.log(data);
        io.sockets.emit('receiveMsg', data);
    });

    currentSocket.on('joinChannel', function(channel) {
        currentSocket.join(channel); // join the room through socket io

        connectedChannel.push(channel); // add a new channel to the connected stack

        if(chan = getChannel(channelStack, channel)) {
           chan.addUser(currentUser);
        } else {
            newChannel = new Channel(channel);
            channelStack.push(newChannel);
            newChannel.addUser(currentUser);
            newChannel.addAdmin(currentUser);
        }

        io.sockets.in(channel).emit('newchanneluser', {
            channel: channel,
            user: currentUser.nickname,
            message: currentUser.nickname + ' has joined channel #' + channel
        });

    });

    currentSocket.on('usernameChange', function(newInfo){
        currentUser.nickname = newInfo.newOne;
        io.sockets.emit('nickChange', newInfo);
    })

    currentSocket.on('newMsg', function(msgInfo) {
        io.sockets.in(msgInfo.channel).emit('newMsg', {
            user: currentUser.nickname,
            message: msgInfo.message,
            channel: msgInfo.channel
        });
    });

    // Current User Var

    currentSocket.on('register', function(udata) {
        register(udata);
        currentSocket.emit('register', 'You have been registered');
    });

    /**
     * When the using login through the form
     */

    currentSocket.on('login', function(data) {
        var sql = initSql();
        var query = "SELECT * FROM users WHERE email = " + sql.escape(data.email) + " AND password = '" + md5(sql.escape(data.password)) + "'";
        sql.query(query, function(error, result) {
            var res = false;
            if(!error) {
                if(result.length == 1) {
                    result = result[0];
                    result.password = undefined;

                    res =  result;
                }
            }

            currentSocket.emit('login', res);
        });
    });

    /**
     * When the user arrives on index.html after the login form
     */

    currentSocket.on('session', function(data) {

        console.log('new user connected ' + data.nickname);

        connectedUser[data.id] = data;
        currentUser = new user(data.id, data.email, data.nickname, data.password);
        // say the user he's connected
        currentSocket.emit('session', 'ok');

        // say all user a new people has connected
        io.sockets.emit('new_user', { nick: currentUser.nickname, id: currentUser.id });

    });

    /**
     * When the socket is closed
     */

    currentSocket.on('disconnect', function() {
        if(!currentUser)
            return false;
        io.sockets.emit('userquit', { nick: currentUser.nickname, id: currentUser.id });
        delete connectedUser[currentUser.id];
    });
});
