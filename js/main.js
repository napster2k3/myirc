/**
 * Created by camillegreselle on 06/07/2014.
 */

$(document).ready(function (){

    if(sessionStorage.auth == null)
        window.location.href = 'login.html';

    session = $.parseJSON(sessionStorage.auth);
    var socket = io.connect('http://localhost:1337');

    /**
     * Array containing channels the user is connected to
     * @type {Array}
     */

    activeChannel = [];

    /**
     * Current Chanel focused on User Interface
     * @type {string}
     */

    currentChannel = false;

    socket.emit('session', session);
    socket.on('session', function(res) {

    });

    socket.on('userquit', function(res){
        $.notify( res.nick + " has disconnected !", "warn");
    });

    socket.on('new_user', function(user){
        $.notify( user.nick + " is online !", "success");
    });

    /**
     * Autodraw homeChannel
     * @function & type
     */

    var drawChannel = function(channel) {

        var $div = $('#' + channel);
        var $channelContainer = $('#channelContainer');

        if($div.length <= 0) { // if the channel is not loaded / nor running in background
            if(currentChannel) {
                $('#' + currentChannel).hide();
            }
            $channelContainer.append('<div id="' + channel + '"></div>');
            $('#' + channel).load('channel.view.html', function(err) {

            });
            $('#onChannel').html('Active on #' + channel);

        } else {
            if(currentChannel) {
                $('#' + currentChannel).hide();
            }

            $div.fadeIn();
        }

        currentChannel = channel;
    };

    var removeChannel = function(channel) {
        $('#' + channel).remove();
        for (var i in activeChannel)
            if(activeChannel[i] == channel)
                delete activeChannel[i];
        // emit to users in the channel that user disconnected
    };

    socket.on('newchanneluser', function(info) {
        $.notify(info.message, "info");
    });

    /**
     * Command Manager / Message Sender
     */

    $('#msgForm').submit(function(e) {
        e.preventDefault();

        var cmd = [ '/nick ', '/list ', '/join ', '/part ', '/users', '/switch ', '/wizz', '/msg ', '/kick '];
        var msg = $('#msgBox').val();
        if(msg.length == 0)
            return false;

        for (var i in cmd) {
            match = msg.match(cmd[i]);
            console.log(match);
            if(match != null ) {
                cmdInfo = match.input.split(" ");

                switch(cmdInfo[0]) {
                    case '/join' :
                        socket.emit('joinChannel', cmdInfo[1]);
                        activeChannel.push(cmdInfo[1]);
                        drawChannel(cmdInfo[1]);
                    break;

                    case '/part' :
                        removeChannel(cmdInfo[1]);
                        socket.emit('leaveChannel', cmdInfo[1]);
                        console.log(activeChannel);
                    break;

                    case '/users' :
                        socket.emit('getUserList', currentChannel);
                        socket.on('fetchUserList', function(data) {
                           for(var i in data) {
                               if(data[i] != null)
                                   $.notify(data[i].nickname + ' [UserListQuery]', "info");
                           }
                        });
                    break;

                    case '/list' :
                        socket.emit('getChannelList', cmdInfo[1]);
                    break;

                    case '/nick' :
                        socket.emit('usernameChange', {old: session.nickname, newOne: cmdInfo[1] })
                        session.nickname =  cmdInfo[1];
                    break;

                    case '/msg' :
                        socket.emit('sendMsg', {toUser: cmdInfo[1], msg: cmdInfo[2]});
                    break;

                    case '/switch' :
                        drawChannel(cmdInfo[1]);
                        $('#onChannel').html('Active on #' + cmdInfo[1]);
                    break;

                    case '/wizz' :
                        socket.emit('wizz', currentChannel);
                    break;

                    case '/kick' :
                        socket.emit('kickUser', {user: cmdInfo[1], room: currentChannel});
                        $.notify(cmdInfo[1] + ' has been kicked from the room');
                    break;
                }
                $('#msgBox').val('');
                return false;
            }
        }
        socket.emit('newMsg', { message: msg, channel: currentChannel });
        //$($('#' + currentChannel).children().children()[1]).children().append('<li class="out"><img class="avatar" alt="" src=""><div class="message"><span class="arrow"></span><a href="#" class="name">'+session.nickname+'</a><span class="datetime"> at 20:54 </span><span class="body">'+$('#msgBox').val()+'</span></div></li>');
        $('#msgBox').val('');
        return false;
    });

    /**
     * Message Receiving
     */

    socket.on('newMsg', function(data) {
        var msgClass = data.user == session.nickname ? "out" : "in";
        $($('#' + data.channel).children().children()[1]).children().append('<li class="'+msgClass+'"><img class="avatar" alt="" src=""><div class="message"><span class="arrow"></span><a href="#" class="name">'+data.user+'</a><span class="datetime"> at 20:54 </span><span class="body">'+data.message+'</span></div></li>');
    });

    /**
     * Wizz Receiving
     */

    socket.on('pushWizz', function(channel) {
        $('#' + channel).effect( "shake" );
    });

    /**
     * UserChangeNickName Notify
     */

    socket.on('nickChange', function(data) {
        $.notify(data.old + ' has changed his nickname to ' + data.newOne);
    });

    /**
     * Receive Message
     */

    socket.on('receiveMsg', function(data) {
        if(data.toUser == session.nickname)
            alert('Message de: ' + data.toUser + "\n\n" + data.msg);
    });

    /**
     * Fetch Channel List
     */

    socket.on('fetchChannelList', function(cStack) {
        for(var i in cStack)
            $.notify(cStack[i] + ' [ChannelListQuery]', 'info');
    });

});