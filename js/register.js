/**
 * Created by camillegreselle on 05/07/2014.
 */

$(document).ready(function (){

    var socket = io.connect('http://localhost:1337');

    /**
     * Register Section
     */

    $('#register').submit(function(e){
        e.preventDefault();
        socket.emit('register', {
            email: $("input[name='email']").val(),
            password: $("input[name='password']").val(),
            nickname: $("input[name='nickname']").val()
        });
    });

    socket.on('register', function(res) {
        alert(res);
        setTimeout(function() {
            window.location.href = 'login.html';
        }, 3000);
    })
});