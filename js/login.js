/**
 * Created by camillegreselle on 05/07/2014.
 */

$(document).ready(function (){

    /**
     * Session Check
     */

    if(sessionStorage.getItem('auth') != null)
        window.location.href = 'index.html';

    var socket = io.connect('http://localhost:1337');

    /**
     * Login Section
     */

    $('#login').submit(function(e){
        e.preventDefault();
        socket.emit('login', {
            email: $("input[name='email']").val(),
            password: $("input[name='password']").val()
        });
    });

    socket.on('login', function(res) {
        if(typeof(res) == "object") {
            sessionStorage.setItem('auth', JSON.stringify(res));
            window.location.href = 'index.html';
        } else {
            alert('Mauvais identifiant !');
        }
    });
});